
package armstrongnumbers;

import java.util.ArrayList;

/************************************************
 *              ArmstrongNumbers                *
 *                                              *
 *      Tells you if the number you type        *
 *      is an Armstrong Number or not           *
 *                                              *
 *      Armstrong numbers verify:               *
 * n = sum(pow(everynumber, number of numbers)) *
 *                                              *
 * @author Vincent Milano                       *
 ************************************************/


public class ArmstrongNumbers
{
    public static void main(String[] args)
    {
        String number = "54748";
        
        if (isArmstrongRecursive(number, number.length(), Integer.parseInt(number), number.length()))
            System.out.println("[" + number + "] - Armstrong Number !");
        else 
            System.out.println("[" + number + "] - Uh... No sorry :( !");
    }

    //Using ArrayLists
    private static boolean isArmstrong(String number) 
    {
        int weight  = number.length();
        int result  = 0;
        
        for (int i = 0; i < weight; i++)
            result += times(Character.getNumericValue(number.charAt(i)), weight);
        
        return result == Integer.parseInt(number);
    }
    
    /**
     * Recursive style ;p
     * 
     * @param number    -> String number to guess
     * @param weight    -> Numbers contained by your number
     * @param tmpNumber -> Number that decreases each step 
     * @param tmpWeight -> Step
     * @return 
     */
    private static boolean isArmstrongRecursive(String number, int weight, int tmpNumber, int tmpWeight)
    {
        if (tmpWeight == 0)
            return tmpNumber == 0;
        
        tmpNumber -= times(Character.getNumericValue(number.charAt(weight - tmpWeight)), weight);
        tmpWeight--;
        return isArmstrongRecursive(number, weight, tmpNumber, tmpWeight);
    }
    
    //HomeMade Pow
    private static int times(int value, int weight)
    {
        int result = value;
        
        for (int i = 0; i < weight - 1; i++)
            result *= value;
        
        return result;
    }
}